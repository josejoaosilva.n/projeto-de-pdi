import numpy as np
import math
from fractions import Fraction
import cv2
from copy import deepcopy
from . import interface

#// daqui pra baixo são umas variáveis que são necessárias para o processamento, algumas delas só serão usadas aqui, outras não.
#// tem-se de ver onde elas ficarão para saber se necessitará ou não explicitar, dentro de uma função, se elas são globais
#// do jeito que eu testei, que foi com elas assim, e só precisei explicitar, em process(), que "fails_all" é global

# Velocidade  do vídeo, em pixels/frame, que é fornecida externamente
vel = 3.46

#// ^ variáveis necessárias para as funções
########################################################################################################################################
#// Variáveis apenas do processamento

# Parametro usado para saber se deve colocar ou não a torrada na lista, ver eh_nova()
lim = 20 #//eu não sei explicar isso... mas só funciona assim... tô confiando no Esteves, pq ele pediu =)

# lista com todas as torradas, que será passado para a interface, esta é a variável de retorno da função process()
fails_all = []

# Nº de torradas totais passadas pela esteira
count_fails = 0

# Nº de torradas atualmente na esteira que estão fora do padrão 
count_fails_tela = 0


def reset_variaveis():
    global fails_all, count_fails, count_fails, count_fails_tela
    fails_all = []
    count_fails = 0
    count_fails_tela = 0

#// ^^ Variáveis apenas do processamento
########################################################################################################################################
#Classe para as torradas
class Torrada:
    
    def __init__(self, box=0, angulo=0, centroide=0, alvo = False, vel=3.46):
        self.box = box
        self.ang = angulo
        self.centroide = centroide
        self.alvo = alvo
        self.velocidade = vel
        self.shift = self.calc_shift(vel)
     
   

    def get_velocidade(self):
        return self.velocidade
    
    def get_shift(self):
        return self.shift
    
    def set_box(self, box):
        self.box = box
    
    def set_centroide(self, centroide):
        self.centroide = centroide
    
    def set_velocidade(self, velocidade):
        self.velocidade = velocidade
    
    def set_shift(self, shift):
        self.shift = shift

    def serialize(self):
        return [self.centroide[0], self.centroide[1], self.ang]

    def calc_shift(self, velocidade):
    
        teto = math.ceil(velocidade)
        piso = math.floor(velocidade)

        resto = round((velocidade-piso) , 2)

        frac = Fraction(resto).limit_denominator()

        tamanhoProp = frac.denominator  # soma da proporção de nTeto e nPiso
        nTeto = frac.numerator # proporção do teto(ceil)
        nPiso = tamanhoProp - nTeto # proporção do piso(floor)

        array_teto = np.ones(nTeto)*teto
        array_piso = np.ones(nPiso)*piso
                
        output = np.ones(array_teto.size + array_piso.size)
        
        count_teto = 0
        count_piso = 0
        turn_teto = True        
        
        # Intercalando os valores do shift
        for i in range(0,output.size):
            
            if count_teto == array_teto.size:
                output[i] = array_piso[count_piso]
                count_piso = count_piso + 1
                
            elif count_piso == array_piso.size:
                output[i] = array_teto[count_teto]
                count_teto = count_teto + 1
                
            else:
                if turn_teto:
                    output[i] = array_teto[count_teto]
                    count_teto = count_teto + 1
                    turn_teto =  False
                else:
                    output[i] = array_piso[count_piso]
                    count_piso = count_piso + 1
                    turn_teto =  True

        return output

def velocidade(cm):
    vel = 0
    # falta ser implementada, ela vai ser usada para calcular, periodicamente, a velocidade da esteira
    # Go Pedros !
    #// a ideia é pegar ela por meio de uma medição externa, e atualizar a variável global vel
    return vel
########################################################################################################################################
def process(img):
    output = deepcopy(img)
    jeans = cv2.blur(output, (13,13))
    _, jeans = cv2.threshold(jeans, 20, 255, cv2.THRESH_BINARY)
    edges = cv2.Canny(jeans, 50, 100)
    contours, _ = cv2.findContours(edges, cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
    coordinates = []
    for idx, contour in enumerate(contours):
        length = cv2.arcLength(contour, closed=True)
        if length < 260: continue

        x,y,w,h = cv2.boundingRect(contour)
        coordinates.append([x,y,w,h])
        
    return {
        'fails': coordinates,
    }
